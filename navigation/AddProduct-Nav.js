import React, {useEffect, useState, useLayoutEffect} from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Sell from '../Screens/Sell';
import { Pressable, Button, Image } from 'react-native';
import Category from '../Screens/Category';
import Brand from '../Screens/Brand';

const AddProductNav = ({navigation}) => {

  const AddProductStack = createNativeStackNavigator();
  useLayoutEffect(() => {
    navigation.setOptions({tabBarStyle: {display: 'none'}})
  })
  return (
    <AddProductStack.Navigator >
      <AddProductStack.Screen name="Sell" component={Sell} options={{
          headerLeft: () => (
            <Pressable onPress={() => navigation.goBack()} title="Info" color="black" >
                <Image style={{resizeMode: 'contain', width:20, height:20}} source={require('../assets/back.png')} />
            </Pressable>
          ),
          headerTintColor: '#EC146D',
        }}  />
      <AddProductStack.Screen name="Category" component={Category} />
      <AddProductStack.Screen name="Brand" component={Brand}  />
    </AddProductStack.Navigator>
  )
};
export default AddProductNav;