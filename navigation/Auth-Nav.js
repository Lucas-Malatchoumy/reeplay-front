import React, {useEffect, useState} from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Auth from '../Screens/Auth';
import SignIn from '../Screens/Sign-in';
import SignUp from '../Screens/Sign-up';
import AppNav from './App-Nav';

const AuthNav = () => {

  const Authstack = createNativeStackNavigator();
  return (
    <Authstack.Navigator>
      <Authstack.Screen name="Home" component={Auth} />
      <Authstack.Screen name="SignUp" component={SignUp}  />
      <Authstack.Screen name="SignIn" component={SignIn}  />
      <Authstack.Screen name="App" component={AppNav} options={{ header: () => null }} />
      {/* <Authstack.Screen name="Details" component={Details} options={{ header: () => null }} />
      <Authstack.Screen name="Payment" component={Payment} options={{ header: () => null }} /> */}
    </Authstack.Navigator>
  )
};
export default AuthNav;