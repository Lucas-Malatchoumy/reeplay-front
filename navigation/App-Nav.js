import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {SafeAreaView, Text, Pressable, Image, TouchableOpacity, View, StyleSheet} from 'react-native'
import User from '../components/user';
import Heart from '../components/heart';
import Message from '../components/message';
import ShopNav from './Shop-Nav';
import AddProductNav from './AddProduct-Nav'
const Tab = createBottomTabNavigator();

const Create = ({children, onPress}) => {
  return (
    <TouchableOpacity style={{top: -50}} onPress={onPress}>
      {children}
    </TouchableOpacity>
  )
}

const AppNav = () => {
    return (
        <Tab.Navigator screenOptions={{
            tabBarStyle: {...styles.tabs},
            tabBarShowLabel: false,
            headerShown: false,
            }}>
            <Tab.Screen name='ShopNav' component={ShopNav} options={{
              tabBarIcon: ({focused}) => {
                return (
                    <Image style={styles.icons} source={focused ? require('../assets/shopping-cart.png') : require('../assets/shopping-cart-1.png')} />
                )
              }
            }}>
            </Tab.Screen>
            <Tab.Screen name='Heart' component={Heart} options={{
              tabBarIcon: ({focused}) => {
                return (
                    <Image style={styles.icons} source={focused ? require('../assets/heart-solid.png') : require('../assets/heart-solid-1.png')} />
                )
              }
              //tabBarStyle: {width: 40}
            }}>
            </Tab.Screen>
            <Tab.Screen name='AddProductNav' component={AddProductNav} options={{
              tabBarIcon: () => {
                return (
                  <Image style={{height: 60, width: 60}} source={require('../assets/create.png')} />
                )
              },
              tabBarButton: (props) => {
                return (
                  <Create {...props}/>
                )
              }
            }} />
            <Tab.Screen name='Message' component={Message} options={{
              tabBarIcon: ({focused}) => {
                return (
                    <Image style={styles.icons} source={focused ? require('../assets/message.png') : require('../assets/message-1.png')} />
                )
              }
            }} />
            <Tab.Screen name='User' component={User} options={{
              tabBarIcon: ({focused}) => {
                return (
                    <Image style={styles.icons} source={focused ? require('../assets/user.png') : require('../assets/user-1.png')} resizeMode="contain" />
                )
              },
            }}>
            </Tab.Screen>
        </Tab.Navigator>
    )
};

export default AppNav;

const styles = StyleSheet.create({
    tabs: {
      position: 'absolute',
      elevation: 0,
      backgroundColor: 'white',
      borderTopLeftRadius: 30,
      borderTopRightRadius: 30,
      height: 90,
      justifyContent: 'center',
      alignItems: 'center',
      shadowOffset: {width: 0, height: -2},
      shadowColor: '#D8D8D8',
      shadowOpacity: 0.5,
    },
    icons: {
        width: 30,
        height: 30,
        resizeMode: 'contain'
    }
  });