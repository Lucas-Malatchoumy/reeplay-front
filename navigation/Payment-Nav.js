import React, {useEffect, useState, useLayoutEffect} from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Details from '../Screens/Details'
import Payment from '../Screens/Payment';
import { Pressable, Image } from 'react-native';

const PaymentNav = ({navigation}) => {
  useLayoutEffect(() => {
    navigation.setOptions({tabBarStyle: {display: 'none'}})
  })

  const PaymentStack = createNativeStackNavigator();
  return (
    <PaymentStack.Navigator screenOptions={{tabBarStyle: {display: 'none'}}}>
      <PaymentStack.Screen name="Details" component={Details} options={{
          headerLeft: () => (
            <Pressable onPress={() => navigation.goBack()} title="Info" color="black" >
                <Image style={{resizeMode: 'contain', width:20, height:20}} source={require('../assets/back.png')} />
            </Pressable>
          ),
          headerTintColor: '#EC146D'
        }}  />
      <PaymentStack.Screen name="Payment" component={Payment}  options={{
          headerLeft: () => (
            <Pressable onPress={() => navigation.goBack()} title="Info" color="black" >
                <Image style={{resizeMode: 'contain', width:20, height:20}} source={require('../assets/back.png')} />
            </Pressable>
          ),
          headerTintColor: '#EC146D'
        }}  />
    </PaymentStack.Navigator>
  )
};
export default PaymentNav;