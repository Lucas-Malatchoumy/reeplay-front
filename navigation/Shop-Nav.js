import React, {useEffect, useState} from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Shop from '../Screens/Shop';
import PaymentNav from './Payment-Nav';

const ShopNav = () => {

  const ShopNav = createNativeStackNavigator();
  return (
    <ShopNav.Navigator>
      <ShopNav.Screen name="Shop" component={Shop}/>
      <ShopNav.Screen name="PaymentNav" component={PaymentNav} options={{ header: () => null }} />
    </ShopNav.Navigator>
  )
};
export default ShopNav;