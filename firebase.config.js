import firebase from "firebase/compat";
import 'firebase/compat/auth';
import 'firebase/compat/storage';
import 'firebase/compat/firestore';
import Config from "react-native-config";

const firebaseConfig = {
  apiKey: Config.APIKEY,
  authDomain: "reeplay-e0320.firebaseapp.com",
  projectId: "reeplay-e0320",
  storageBucket: "reeplay-e0320.appspot.com",
  messagingSenderId: "814270676655",
  appId: "1:814270676655:web:0e08c7471d9478b4a8de5a"
};

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
};

export {firebase}