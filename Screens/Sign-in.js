import React, {useState} from 'react';
import * as SecureStore from 'expo-secure-store';
import axios from 'axios';
import {SafeAreaView, ScrollView, Text, Button, View, StyleSheet} from 'react-native'
import Input from '../components/Input';
import Btn from '../components/Button';
const SignIn = ({navigation}) => {

  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("");
  const [errors, setErrors] = useState({});
  
  const login = () => {
    const data = {email, password}
    axios.post(`http://vps-e7f2f466.vps.ovh.net:3001/Reeplay/user/login`, data ).then((response) => {
        if (response.data.error) {
            alert(response.data.error);
        }
        else {
          SecureStore.setItemAsync('token', response.data);
          navigation.navigate('App');
        }
    });
  }
  
return(
    <SafeAreaView style={styles.container}>
       <ScrollView >
          <Input change={(text) => {setEmail(text)}}
            label="Email"
            name="email"
            placeholder="email"         
          />
          <Input change={(text) => {setPassword(text)}}
          label="Password"
            name="password"
            placeholder="Password"
            secureTextEntry
          />
        <View style={styles.button}>
          <Btn text={"Se connecter"} onPress={login}></Btn>
        </View>
      </ScrollView>     
    </SafeAreaView>
  )
}

export default SignIn

const styles = StyleSheet.create({
  container: {
    marginTop: '50%',
    width: '90%',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  button: {
    marginTop: 20,
    width: '100%',
    alignItems: 'center'
  },
  test: {
    fontSize: 10,
    textDecorationLine: 'none'
  }
});