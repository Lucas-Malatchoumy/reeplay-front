import React, {useState} from 'react'
import {SafeAreaView, Dimensions, TouchableOpacity, Text, Button, StatusBar, Image, View, StyleSheet} from 'react-native';
import Dispatch from '../components/Dispatch';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height
const Payment = ({navigation, route}) => {
    let product = route.params;
    delete product.description;
    return (
        <SafeAreaView style={{paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,}}>
          <View style={styles.product}>
            <View style={{borderWidth: 1, borderColor: '#EC146D'}}>
                <Image style={styles.image} source={{uri: product.images[0]}}></Image>
            </View>
            <View style={styles.info}>
                <Text style={{fontSize: 20}}>{product.name}</Text>
                <Text>{product.category}</Text>
                <Text style={{color: '#EC146D'}}>{product.state}</Text>
                <Text style={{textAlign: 'right', fontSize: 20}} >{product.price}</Text>
            </View>
          </View>
          <View>
            <Dispatch />
            <Dispatch />
          </View>
        </SafeAreaView>
    )
};
export default Payment;

const styles = StyleSheet.create({
    product: {
        marginTop: 25,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: 'grey',
        paddingBottom: 25,
        borderBottomWidth: 1,
        paddingHorizontal: 50,
        height: 130,
    },
    info: {
        width: '60%',
        justifyContent: 'space-between',
        marginLeft: Platform.OS === "android" ? 15 : 20,
        paddingRight: 10,
    },
    image: {       
        width: 100,
        height: 100,
        resizeMode: 'contain',
    }
})