import React, { useState, useEffect } from 'react';
import { SafeAreaView, ScrollView, TouchableOpacity, Dimensions, Text, Image, View, StyleSheet, Platform, ImageStore } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import * as SecureStore from 'expo-secure-store';
import Dispatch from '../components/Dispatch';
import Input from '../components/Input';
import Btn from '../components/Button'
import CustomTextArea from '../components/TextArea';
import BouncyCheckbox from 'react-native-bouncy-checkbox';
import axios from 'axios';
import DateTimePicker from '@react-native-community/datetimepicker';
import {firebase} from '../firebase.config';

/**
 * 
 * @param {*} param0 
 * @returns 
 */
const ImagePickerExample = ({navigation, route}) => {
  const width = Dimensions.get('window').width;
  const height = Dimensions.get('window').height
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [category, setCategory] = useState()
  const [brand, setBrand] = useState();
  const [price, setPrice] = useState()
  const [images, setImage] = useState([]);
  const [checkboxState, setCheckboxState] = useState(false);
  const [date, setDate] = useState(new Date())


  useEffect(() => {
    if (route.params?.brand) {
      setBrand(route.params.brand);
    }
    console.log(brand);
  }, [route.params?.brand])

  useEffect(() => {
    if (route.params?.category) {
      setCategory(route.params.category);
    }
    console.log(category);
  }, [route.params?.category])

  // choose an image in phone gallery
  const pickImage = async () => {
    const permissionResult = await ImagePicker.requestMediaLibraryPermissionsAsync();
    if (permissionResult.granted === false) {
      const get = await ImagePicker.getMediaLibraryPermissionsAsync();
      alert("error");  
    }
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setImage([...images, result])
    }
  };

  // ulpoad multiple images to firebase storage
  const uplaodImage = async (id) => {
    const promises = [];
    images.map(async (image) => {
      const response = await fetch(image.uri);
      const filename = image.name;
      const blob = await response.blob();
      const ref = firebase.storage().ref().child(`${id}/${filename}`).put(blob);
      promises.push(ref);
    });
    Promise.all(promises).then(() => navigation.goBack()).catch((error) => console.log(error));
  }

  // insert product in database
  const createProduct = async () => {
    for (let index = 0; index < images.length; index++) {
      images[index].name = `image_${index}`;  
    }
    const token = await SecureStore.getItemAsync('token');
    const data = {name, description, brand, category, price, images}
    axios.post(`http://vps-e7f2f466.vps.ovh.net:3001/Reeplay/product/create`, data, {
      headers: {
        token: token
      }
    }).then((response) => {
      if (response.data.error) {
        alert(response.data.error);
      }
      else {
        uplaodImage(response.data.id)
      }
    })
  }

  return (
    <SafeAreaView>
        <ScrollView>
        <TouchableOpacity style={styles.container} title='toto' onPress={pickImage}>
            <Image style={styles.picker}  source={require('../assets/create.png')}/>
        </TouchableOpacity>
        {images && <ScrollView style={{width: width, height: height * 0.35,}} showsHorizontalScrollIndicator={false}
          pagingEnabled
          horizontal>{images.map((image) => <Image key={image.name} source={{ uri: image.uri }} style={{ width: 200, height: 200 }}/>)}</ScrollView>}
        <View style={{marginHorizontal: 20, marginBottom: 20}}>
        <Input
            change={(text) => {setName(text)}}
            value={name}
            label="Titre"
            name="Titre"
            maxLength={10}
          />
          <CustomTextArea
            change={(text) => {setDescription(text)}}
            label="Description"
            value={description}
            name="Description"
            lignes={10} 
          />
        </View>
        <Dispatch complete={category ? true : false} text="Catégorie" onPress={() => navigation.navigate('Category')} />
        <Dispatch text="Age" onPress={() => navigation.navigate('Subpage', {title: 'age'})} />
        <Dispatch test={setBrand} complete={brand ? true : false} text="Marque" onPress={() => navigation.navigate('Brand')} />
        <View style={styles.activeBid}>
          <Text style={{fontSize: 12}}>Je souhaite vendre mon produit aux enchères</Text>
          <BouncyCheckbox  fillColor='#EC146D' onPress={() => {setCheckboxState(!checkboxState)}} />
          {checkboxState && <Text>Hello</Text>}
        </View>
        {/* <View>
          <DateTimePicker value={new Date()} mode="date" display="default" />
          <DateTimePicker value={new Date()} mode="time" display="default" dayOfWeekFormat={'{dayofweek.abbreviated(2)}'} />
        </View> */}
        <View style={{marginHorizontal: 20, marginBottom: 20}}>
          <Input
          change={(text) => {setPrice(text)}}
          label="Prix"
          name="Prix"
          value={price} />
        </View>
        <View style={styles.button}>
            <Btn text={"Mettre en vente"} onPress={createProduct}></Btn>
        </View>
        </ScrollView>
    </SafeAreaView>
  );
}
export default ImagePickerExample
const styles = StyleSheet.create({
    container: {
      width: '100%',
      height: 250,
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: 1,
      borderColor: '#EC146D'
    },
    button: {
      marginTop: 20,
      width: '100%',
      alignItems: 'center'
    },
    picker: {
      resizeMode: 'contain',
      height: 50,
      width: 50
    },
    activeBid: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginHorizontal: 20
    }
})