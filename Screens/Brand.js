import React, { useState, useEffect } from 'react';
import { SafeAreaView, ScrollView, TouchableOpacity, StatusBar, Text, Image, View, StyleSheet, Platform, ImageStore } from 'react-native';
import axios from 'axios';
import Btn from '../components/Button'
import BouncyCheckbox from 'react-native-bouncy-checkbox';

const Brand = ({navigation}) => {
  const [brands, setBrands] = useState([]);
  const [checkboxState, setCheckboxState] = useState();
    useEffect(() => {
        getBrands()
    }, [checkboxState])

    const getBrands = () => {
      axios.get('http://vps-e7f2f466.vps.ovh.net:3001/Reeplay/brands/get')
        .then((response) => {
            setBrands(response.data);
        });
    }
    return (
      <ScrollView >
        <View style={styles.brands}>
          {brands.map((brand) => {
          return (
            <View style={styles.brand} key={brand.id}>
              <Text>{brand.brand}</Text>
              <BouncyCheckbox isChecked={checkboxState == brand.id ? true : false} disableBuiltInState fillColor='#EC146D' onPress={() => {setCheckboxState(brand.id)}}>
              </BouncyCheckbox>
            </View>
          )
         })}
        </View>
      <Btn text="Valider" onPress={() => navigation.navigate({name: 'Sell', params: {brand: checkboxState}, merge: true})} />
      </ScrollView>
    )
};

const styles = StyleSheet.create({
    brands: {
      marginVertical: 30,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center'
    },
    brand: {
      marginHorizontal: 20,
      flexDirection: 'row',
      justifyContent: 'space-between',
      borderBottomWidth: 1,
      borderBottomColor: '#D8D8D8',
      marginVertical: 2.5,
      paddingVertical: 10
    }
})

export default Brand;