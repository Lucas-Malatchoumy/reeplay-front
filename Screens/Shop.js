import React, {useEffect, useState} from 'react';
import { SafeAreaView, View, StyleSheet, ScrollView, Image, Dimensions, StatusBar } from 'react-native';
import * as SecureStore from 'expo-secure-store';
import ToyCard from '../components/ToyCard';
import Btn from '../components/Button';
import Brands from '../components/Brands';
import PaymentNav from '../navigation/Payment-Nav';
import {firebase} from '../firebase.config';
import { getStorage, ref, getDownloadURL } from 'firebase/storage';
import { useFonts } from 'expo-font';
import axios from 'axios';
import KidMod from '../components/Kid-Mod';
import Config from "react-native-config";

const Shop = ({navigation}) => {
  const [products, setproduct] = useState([]);
  const width = Dimensions.get('window').width;
  const height = Dimensions.get('window').height
  useEffect(() => {
    getProducts();
  }, [])

  const supp = async () => {
    await SecureStore.deleteItemAsync('token');
  }

  const getProducts = () => {
    axios.get('http://vps-e7f2f466.vps.ovh.net:3001/Reeplay/product')
    .then((response) => {
      const products = response.data;
      products.forEach(product => {
        product.images = [];
        firebase.storage().ref().child(`${product.id}/`).listAll()
        .then((response) => {
          response.items.forEach(item => {
            getDownloadURL(item).then((url) => { product.images.push(url); setproduct([...products, product])});

          });
        })
      });
    })
  }

  return (
      <SafeAreaView style={styles.container}>
        <ScrollView>
        <KidMod />
        <ScrollView showsHorizontalScrollIndicator={false}
          pagingEnabled
          horizontal style={styles.popular}>
          {products.map((product) => {
            console.log(product.images);
            return (
              <ToyCard onPress={() => {navigation.navigate('PaymentNav', { screen: 'Details', params: product })}}
                name={product.name}
                key={product.id}
                source={product.images[0]}
                price={product.price}>
                </ToyCard>
            )
          })}
        </ScrollView>
        <Brands />
        </ScrollView>
      </SafeAreaView>
  )
};

export default Shop;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
      flexWrap: 'wrap',
      justifyContent: 'center',
    },
    popular: {
      marginVertical: 20
    },
    logo: {
      width: 250,
      height: 250,
    }
  });