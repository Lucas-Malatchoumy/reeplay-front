import React, {useState} from 'react'
import axios from 'axios';
import * as SecureStore from 'expo-secure-store';
import {SafeAreaView, ScrollView, Text, Button, View, StyleSheet} from 'react-native'
import Input from '../components/Input';
import Btn from '../components/Button';
import BouncyCheckbox from 'react-native-bouncy-checkbox';
import { LinearGradient } from 'expo-linear-gradient';
const SignUp = ({navigation}) => {

  const [first_name, setFirst_name] = useState("");
  const [last_name, setLast_name] = useState("");
  const [username, setUsername] = useState("")
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [password, setPassword] = useState("");
  const [address, setAddress] = useState("");
  const [confirm_password, setConfirmPassword] = useState("")
  const [isDisabled, setDisabled] = useState(true);
  const [errors, setErrors] = useState({});
  const [checkboxState, setCheckboxState] = useState(false);
  
  const register = () => {
    const data = {first_name, last_name, email, password, username, phone, address}
    if (confirm_password != password) {
      alert('passwords are not the same')
    }
    else {
      axios.post(`http://vps-e7f2f466.vps.ovh.net:3001/Reeplay/user/register`, data ).then((response) => {
        if (response.data.error) {
          alert(response.data.error);
        }
        else {
          SecureStore.setItemAsync('token', response.data);
          navigation.navigate('App');
        }
      })
    }
  }
  
return(
    <SafeAreaView style={styles.container}>
       <ScrollView>
        <Input
            change={(text) => {setFirst_name(text)}}
            label="First Name"
            name="firstName"
            placeholder="First Name"
            rules={{required: 'Username is required'}}
          />
          <Input
          change={(text) => {setLast_name(text)}}
            label="Last Name"
            name="lastName"
            placeholder="Last Name"         
          />
          <Input change={(text) => {setUsername(text)}}
            label="Username"
            name="username"
            placeholder="username"         
          />
          <Input change={(text) => {setEmail(text)}}
            label="Email"
            name="email"
            placeholder="Email"         
          />
          <Input change={(text) => {setPhone(text)}}
            label="Phone"
            name="phone"
            placeholder="phone"
            keyboardType="phone-pad"
            maxLength={10}
          />
          <Input change={(text) => {setAddress(text)}}
            label="Address"
            name="address"
            placeholder="address"         
          />
          <Input change={(text) => {setPassword(text)}}
          label="Password"
            name="password"
            placeholder="Password"
            secureTextEntry         
            rules={{
              required: 'Password is required',
              minLength: {
                value: 3,
                message: 'Password should be minimum 3 characters long',
              },
            }}
          />
          <Input change={(text) => {setConfirmPassword(text)}}
          label="Confirm Password"
            name="Confirm_password"
            placeholder="Confirm_password"
            secureTextEntry         
          />
        <BouncyCheckbox fillColor='#EC146D' text="En m'inscrivant, je confirme que j'ai accepté les Conditions Générales d'Utilisation et les Conditions de vente Pro de ReePlay, avoir lu la Politique de Confidentialité, et que j'ai plus de 18 ans." textStyle={styles.test} onPress={() => {setCheckboxState(!checkboxState); setDisabled(!isDisabled)}}>
        </BouncyCheckbox>
        <View style={styles.button}>
          <Btn disabled={isDisabled} text={"S'inscrire"} onPress={register}></Btn>
        </View>
      </ScrollView>
      
    </SafeAreaView>
  )
}

export default SignUp

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '90%',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  button: {
    marginTop: 20,
    width: '100%',
    alignItems: 'center'
  },
  test: {
    fontSize: 10,
    textDecorationLine: 'none'
  }
});