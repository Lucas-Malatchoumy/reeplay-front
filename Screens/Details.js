import React, {useState, useEffect} from 'react';
import { View, SafeAreaView, ScrollView, Image, Text, StyleSheet, TouchableOpacity, StatusBar} from 'react-native';
import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';
import Btn from '../components/Button';
import Btn1 from '../components/Button-white';
import ProductInfo from '../components/Product-Info';
import Swiper from '../components/Swiper-Images';


/**
 * 
 * @param {*} param0 
 * @returns 
 */
const Details = ({navigation, route}) => {
    const product = route.params;
    const tabBarHeight = useBottomTabBarHeight();

    return (
        <SafeAreaView style={[styles.container, {marginBottom: tabBarHeight}]}>
            <ScrollView contentContainerStyle={{alignItems: 'center'}}>
            <Swiper product={product} />
            <ProductInfo product={product} />
            <Btn text={"Acheter"} onPress={() => navigation.navigate('Payment', product)} ></Btn>
            <Btn1 text={"Envoyer un message"}></Btn1>
            </ScrollView>
        </SafeAreaView>
    )
};
export default Details;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    logo: {
      width: 250,
      height: 250,
    },
    info: {
        
    }
  });