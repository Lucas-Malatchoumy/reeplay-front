import React, { useState, useEffect } from 'react';
import { SafeAreaView, ScrollView, TouchableOpacity, StatusBar, Text, Image, View, StyleSheet, Platform, ImageStore } from 'react-native';
import axios from 'axios';
import Btn from '../components/Button'
import BouncyCheckbox from 'react-native-bouncy-checkbox';

const Category = ({navigation}) => {
  const [categories, setCategories] = useState([]);
  const [checkboxState, setCheckboxState] = useState();
    useEffect(() => {
        axios.get('http://vps-e7f2f466.vps.ovh.net:3001/Reeplay/categories/get')
        .then((response) => {
            setCategories(response.data);
        })
    }, [checkboxState])
    return (
      <ScrollView>
        <View style={styles.categories}>
          {categories.map((category) => {
          return (
            <View style={styles.category} key={category.id}>
              <Text>{category.category_name}</Text>
              <BouncyCheckbox isChecked={checkboxState == category.id ? true : false} disableBuiltInState fillColor='#EC146D' onPress={() => {setCheckboxState(category.id)}} />
            </View>
          )
        })}
        </View>
      <Btn text="Valider" onPress={() => navigation.navigate({name: 'Sell', params: {category: checkboxState}, merge: true})} />
      </ScrollView>
    )
};

const styles = StyleSheet.create({
  categories: {
    marginVertical: 30
  },
  category: {
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: '#D8D8D8',
    marginVertical: 2.5,
    paddingVertical: 10
  }
})

export default Category;