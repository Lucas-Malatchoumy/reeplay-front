import { View, Image, StyleSheet} from 'react-native';
import Btn from '../components/Button';
import Btn1 from '../components/Button-white';

  const Auth = ({navigation}) => {
    return (
      <View style={styles.container}>
        <Image style={styles.logo} source={require('../assets/reeplay.png')} />
        <Btn text={"S'inscrire"} onPress={() => navigation.navigate('SignUp')}></Btn>
        <Btn1 text={"Se connecter"} onPress={() => navigation.navigate('SignIn')}></Btn1>
      </View>
    )
  };
export default Auth;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center',
    justifyContent:'center',
  },
  logo: {
    width: 250,
    height: 250,
  }
});