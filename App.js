import React, {useEffect, useState} from 'react';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as SecureStore from 'expo-secure-store';
import AppNav from './navigation/App-Nav';
import { View, Text, StyleSheet } from 'react-native';
import AuthNav from './navigation/Auth-Nav';

const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
  },
};
const Stack = createNativeStackNavigator();
const App = () => {
  const [isLoggin, setIsLoggin ] = useState(true);


  useEffect(() => {
    const test = async () => {
      let token = await SecureStore.getItemAsync('token');
      if (!token) {
        setIsLoggin(false)
      }
    }
    test();
  }, [])
  return (
      <NavigationContainer theme={MyTheme}>
      {isLoggin
        ? <AppNav style={{backgroundColor : "white"}} />
        : <AuthNav style={{backgroundColor : "white"}} />}
      </NavigationContainer>
      
  );
};

export default App;
