const products = [
    {
        id: '1',
        name: 'Renifleurs',
        image: require('./assets/Jjouet.jpeg'),
        category: 'enfant',
        state: 'bon état',
        description: 'je suis la description',
        price: '70€'
    },
    {
        id: '2',
        name: 'Paymobil',
        image: require('./assets/playmobil.jpeg'),
        category: 'adulte',
        state: 'neuf',
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem",
        price: '50€'
    }
];
export default products;