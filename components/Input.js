import React from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';

const CustomInput = (props) => {
  return (

  <View>
    <View
    control={props.control}
    name={props.name}
    rules={props.rules}
      style={[
        styles.container,
        {borderColor: props.error ? 'red' : '#e8e8e8'},
      ]}>
      <Text>{props.label}</Text>
      <TextInput
        onChangeText={props.change}
        value={props.value}
        placeholder={props.placeholder}
        style={styles.input}
        secureTextEntry={props.secureTextEntry}
        keyboardType={props.keyboardType}
        maxLength={props.maxLength}
        autoCapitalize = 'none'
      />
    </View>
    {props.error && (
      <Text style={{color: 'red', alignSelf: 'stretch'}}>{props.error.message || 'Error'}</Text>
    )}
  </View>
  );
};

const styles = StyleSheet.create({
  container: {
    
    width: '100%',
    marginTop: 15,
  },
  input: {
    backgroundColor: '#F0F0F0',
    borderBottomColor: '#F0F0F0',
    borderRadius: 10,
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginVertical: 5,
  },
});

export default CustomInput;