import {TouchableOpacity, Text, Button, StatusBar, Image, View, StyleSheet} from 'react-native';

const KidMod = () => {
    return (
      <View >
        <TouchableOpacity style={styles.container}>
        <Image style={styles.image} source={require('../assets/kid_mod.png')}/>
        <View style={styles.text}>
          <Text style={{fontSize: 18, marginBottom: 20}}>Mode enfant</Text>
          <Text>Activer le mode enfant pour que votre petit rejeton puisse ce balader sur l'App sans risque</Text>
        </View>
        </TouchableOpacity>
      </View>
    )
};

export default KidMod;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    image: {
        resizeMode: 'contain',
        width: 100, 
        height: 100,
        marginRight: 20
    },
    text :{
      flexDirection: 'column',
      width: 250
    }
})