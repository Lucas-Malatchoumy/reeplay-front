import {SafeAreaView, Dimensions, ScrollView, Text, Image, View, StyleSheet} from 'react-native';
import Button from './Button';
import * as SecureStore from 'expo-secure-store';

const User = ({navigation}) => {
    const supp = async () => {
        await SecureStore.deleteItemAsync('token');
        navigation.navigate('Sell')
    }
    return (
        <SafeAreaView>
          <View style={{marginTop: '50%'}}>
            <Button text={"Payment"} onPress={supp}></Button>
          </View>
        </SafeAreaView>
    )
};
export default User;