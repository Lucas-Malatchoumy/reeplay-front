import axios from 'axios';
import React, {useEffect, useState} from 'react';
import { Pressable, StyleSheet, Text, View } from 'react-native';
import ButtonBrand from './Button-brand';

const Brands = () => {
  const [brands, setBrands] = useState([]);

  useEffect(() => {
    getBrands()
  }, [brands])

    const getBrands = () => {
      axios.get('http://vps-e7f2f466.vps.ovh.net:3001/Reeplay/brands/get')
        .then((response) => {
            setBrands(response.data);
        });
    }

    return (
      <View style={styles.conatiner}>
        <Text style={{fontSize: 18, marginBottom: 20}}>Rechercher par maque :</Text>
        <View style={styles.brands}>
          {brands.map((brand) => {
            return (
              <ButtonBrand key={brand.id} text={brand.brand} onPress={() => {alert('zugu')}} />
            )
          })}
        </View>
      </View>
    )
}
export default Brands;

const styles = StyleSheet.create({
    conatiner: {
        width: '80%',
        flexDirection: 'column'
    },
  brands: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'row',
  }
});