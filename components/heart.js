import {SafeAreaView, Dimensions, ScrollView, Text, Button, Image, View, StyleSheet} from 'react-native'

const Heart = () => {
    return (
        <View>
            <Image style={{
                width: 50,

    resizeMode: 'contain'}} source={require('../assets/heart-solid.png')}></Image>
        </View>
    )
};
export default Heart;