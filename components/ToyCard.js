import * as React from 'react';
import { View, SafeAreaView, Image, Text, StyleSheet, TouchableOpacity, StatusBar, Platform} from 'react-native';
import { Dimensions } from 'react-native';
import Favourite from './Favourite';
const {width} = Dimensions.get('screen');
const {cardWith} = width/1.8;

const ToyCard = (props) => {
    return (
        <View style={Platform.OS === "android" ? styles.card : [styles.card, styles.shadow]}>
            <View style={styles.heart}>
              <Favourite /> 
            </View>
            <TouchableOpacity onPress={props.onPress}>
            <Image style={styles.cardImage} source={{uri: props.source}}></Image>
            <View style={{marginHorizontal: 5, paddingTop: 5}}>
              <Text>{props.name}</Text>
              <Text style={styles.price} >{props.price}</Text>
            </View>
            </TouchableOpacity>
        </View>
    )
}

export default ToyCard;

const styles = StyleSheet.create({
    card: {
      height: 250,
      backgroundColor: 'white',
      width: cardWith,
      marginRight: 20,
      borderRadius: 15,
      elevation: Platform.OS === "android" ? 5 : 0      
    },
    shadow: {
      shadowColor: '#171717',
      shadowOffset: {width: 0, height: 5},
      shadowColor: '#D8D8D8',
      shadowOpacity: 0.5,
    },
    heart: {
      position: 'absolute',
      zIndex: 1,
      marginTop: 5,
      marginLeft: 5
    },
    cardImage: {
        height: 200,
        width: 150,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15
        
    },
    logo: {
      width: '100%',
      height: '100%',
      borderRadius: 20,
      resizeMode: 'cover'
    },
    info: {
      height: 75,
      paddingHorizontal: 20,
      justifyContent: 'center'
    },
    price: {
        textAlign: 'right',
        fontSize: 15
    }
  });