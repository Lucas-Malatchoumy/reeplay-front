import {SafeAreaView, Dimensions, TouchableOpacity, Text, Button, StatusBar, Image, View, StyleSheet} from 'react-native';

const Dispatch = (props) => {
    return (
        <TouchableOpacity style={styles.complement} onPress={props.onPress}>
          <View style={props.complete === true ? styles.complete : styles.incomplete} />
          <Text style={styles.text}>{props.text}</Text>
          <Image style={styles.image} source={require('../assets/fleche.png')} />
        </TouchableOpacity>
    )
}

export default Dispatch;

const styles = StyleSheet.create({
    complement: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 50,
        paddingRight: 10,
        marginBottom: 20
    },
    complete: {
        backgroundColor: '#EC146D',
        width: 8,
        height: 50,
        marginRight: 5,
        marginLeft: 1,
        borderTopRightRadius: 8,
    },
    incomplete: {
        backgroundColor: '#D8D8D8',
        width: 8,
        height: 50,
        marginRight: 5,
        marginLeft: 1,
        borderBottomRightRadius: 8
    },
    text: {
        marginLeft: 20
    },
    image: {
        resizeMode: 'contain',
        height: 20,
        width: 20,
        marginLeft: 'auto'
    }
})