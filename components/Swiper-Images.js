import React, {useState} from 'react';
import { View, ScrollView, StyleSheet, Dimensions, Image} from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height

const Swiper = (props) => {
    const [isActive, setIsActive] = useState(0);
    const product = props.product;

    return (
        <View style={styles.scroll}>
          <ScrollView
          showsHorizontalScrollIndicator={false}
          pagingEnabled
          horizontal
          style={styles.scroll}>
            {product.images.map((image) => {
              return (
                <Image style={styles.image} source={{uri: image}} />
              )
            })}
          </ScrollView>
        </View>
    )
};

export default Swiper;

const styles = StyleSheet.create({
    scroll: {
        width: width,
        height: height * 0.55,
        marginBottom: 20
    },
    image: {
        width: width,
        height: 400
    }
})