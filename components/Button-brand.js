import React from 'react';
import { Pressable, StyleSheet, Text, View } from 'react-native';

const ButtonBrand = (props) => {
    return (
    <View style={styles.shadowTop}>
      <View style={styles.shadowBottom}>
      <Pressable disabled={props.disabled} onPress={props.onPress} style={styles.button} title="Press here">
          <Text>{props.text}</Text>
     </Pressable>
    </View>
    </View>
    )
}
export default ButtonBrand;

const styles = StyleSheet.create({
  shadowTop: {
      shadowColor: '#171717',
      shadowOffset: {width: 0, height: -1},
      shadowColor: '#D8D8D8',
      shadowOpacity: 0.5,
  },
    shadowBottom: {
      shadowColor: '#171717',
      shadowOffset: {width: 0, height: 5},
      shadowColor: '#D8D8D8',
      shadowOpacity: 0.5,
    },
    button: {
      backgroundColor: 'white',
      borderRadius: 10,
      paddingHorizontal: 5,
      paddingVertical: 2,
      marginRight: 10,
      marginBottom: 10,
      elevation: Platform.OS === "android" ? 5 : 0
    },
    text: {
      color: 'white',
      fontSize: 16
    }
});