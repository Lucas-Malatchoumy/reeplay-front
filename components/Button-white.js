import React from 'react';
import { Pressable, StyleSheet, Text, View } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

const Button = ({onPress, text}) => {
    return (
      <Pressable onPress={onPress} style={styles.button} title="Press here">
        <LinearGradient start={[0, 1]} end={[1, 1]} colors={['#EF8157','#EC146D']} style={{borderRadius: 20, flex: 1}}>
          <View style={styles.gradient}>
            <Text style={styles.text}>{text}</Text>
          </View>
        </LinearGradient>
     </Pressable>
    )
}
export default Button;

const styles = StyleSheet.create({
    gradient: {
      backgroundColor: "white",
      borderRadius: 20,
      margin: 1,
      flex: 1,
      justifyContent: 'center',
      alignItems:'center',
    },
    button: {
      width: '85%',
      height: 45,
      marginBottom: 20,
    },
    text: {
      textAlign: "center",
      color: '#EC136D',
      fontSize: 16
    }
});