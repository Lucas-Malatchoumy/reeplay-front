import React, {useState} from 'react';
import { View, SafeAreaView, Image, Text, StyleSheet, TouchableOpacity, StatusBar} from 'react-native';

const Favourite  = (props) => {
    const [isFavourite, setIsFavourite] = useState(false);
    return (
        <TouchableOpacity onPress={() => {setIsFavourite(!isFavourite); alert(isFavourite)}} style={[styles.container, props.style]}>
            <Image style={!isFavourite ? styles.heartColor : styles.heart  } source={require('../assets/heart.png')}></Image>
        </TouchableOpacity>
    )

}

export default Favourite;

const styles = StyleSheet.create({
    container: {
        width: 34,
        height: 34,
        borderRadius: 44/2,
        backgroundColor: '#FBFBFB',
        justifyContent: 'center',
        alignItems: 'center'
    },
    heart: {
        height: 15,
        width: 15
    },
    heartColor: {
        tintColor: 'red',
        height: 15,
        width: 15
    }
})