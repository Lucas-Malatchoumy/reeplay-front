import React from 'react';
import { Pressable, StyleSheet, Text } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

const Button = (props) => {
    return (
      <Pressable disabled={props.disabled} onPress={props.onPress} style={styles.button} title="Press here">
        <LinearGradient colors={props.disabled ?  ['#E7E6E6','#DCDBDB'] : ['#EF8157','#EC146D']} start={{ x: 0, y: 1 }}
        end={{ x: 1, y: 1 }} style={styles.gradient}>
            <Text style={styles.text}>{props.text}</Text>
        </LinearGradient>
     </Pressable>
    )
}
export default Button;

const styles = StyleSheet.create({
    gradient: {
      flex: 1,
      justifyContent: 'center',
      alignItems:'center',
      borderRadius: 20
    },
    button: {
      width: '85%',
      height: 45,
      marginBottom: 20,
      marginLeft: 'auto',
      marginRight: 'auto'
    },
    text: {
      color: 'white',
      fontSize: 16
    }
});