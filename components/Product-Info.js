import React, {useState} from 'react';
import { View, Text, StyleSheet} from 'react-native';

const ProductInfo = (props) => {
    const product = props.product;
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{product.name}</Text>
        <Text style={styles.category}>{product.category}</Text>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={styles.state}>{product.state}</Text>
            <Text style={styles.price}>{product.price}€</Text>
        </View>
        <Text style={styles.description}>{product.description}</Text>
      </View>
    )
}

export default ProductInfo;

const styles = StyleSheet.create({
    container: {
        marginBottom: 20,
        width: '80%'
    },
    title: {
      fontSize: 20
    },
    category: {
      color: 'grey'
    },
    state: {
      color: '#EC146D'     
    },
    price: {
        fontSize: 20
    },
    description: {

    }
})